﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;

using Moq;
using NUnit.Framework;

using BorrowBook.Domain.Services;
using BorrowBook.Domain.Entities;

namespace BorrowBook.Test
{
    [TestFixture]
    public class PersonServiceTest
    {
        [Test]
        public void GetPersonBookByBookId_Should_Return_PersonBook()
        {
            var listOfPersonBook = new List<PersonBook>();
            listOfPersonBook.Add(new PersonBook
            {
                Id = "PersonBook01",
                BookId = "Book01",
                PersonId = "Person01",
                Book = new Book
                {
                    Id = "Book01"                    
                },
                Person = new Person
                {
                    Id = "Person01"
                }
            });
            listOfPersonBook.Add(new PersonBook
            {
                Id = "PersonBook02",
                BookId = "Book02",
                PersonId = "Person02",
                Book = new Book
                {
                    Id = "Book02"
                },
                Person = new Person
                {
                    Id = "Person02"
                }
            });

            var personRepMock = new Mock<IEntityRepository<Person>>();
            var personBookRepMock = new Mock<IEntityRepository<PersonBook>>();
            personBookRepMock.Setup(x => x.AllIncluding(It.IsAny<Expression<Func<PersonBook, object>>[]>()))
                .Returns(listOfPersonBook.AsQueryable());

            var personService = new PersonService(personBookRepMock.Object, personRepMock.Object);
            PersonBook personBook = personService.GetPersonBookByBookId("Book02");

            Assert.AreEqual(personBook.BookId, "Book02");
        }
    }
}
