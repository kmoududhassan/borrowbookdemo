﻿using Moq;
using NUnit.Framework;

using BorrowBook.Domain.Services;
using BorrowBook.Domain.Entities;
using BorrowBook.Domain.Exceptions;

namespace BorrowBook.Test
{
    [TestFixture]
    public class BorrowBookTest
    {
        [Test]        
        public void Throws_Exception_When_Person_Does_Not_Exist()
        {   
            //arrange            
            var personServiceMock = new Mock<IPersonService>();
            Person person = null;
            personServiceMock.Setup(x => x.GetPerson(It.IsAny<string>()))
                .Returns(person);
            PersonBook personBook = null;
            personServiceMock.Setup(x => x.GetPersonBookByBookId(It.IsAny<string>()))
                .Returns(personBook);

            var bookRepMock = new Mock<IEntityRepository<Book>>();
            Book book = new Book();
            bookRepMock.Setup(x => x.GetSingle(It.IsAny<string>())).Returns(book);

            //act
            var libaryService = new Libraryervice(bookRepMock.Object, personServiceMock.Object);

            //assert
            Assert.Throws<DoesNotExistException>(() => libaryService.BorrowBook(string.Empty, string.Empty));
        }

        [Test]
        public void Throws_Exception_When_Person_Exist_But_Book_Does_Not()
        {            
            //arrange
            var personServiceMock = new Mock<IPersonService>();
            Person person = new Person();
            personServiceMock.Setup(x => x.GetPerson(It.IsAny<string>()))
                .Returns(person);
            PersonBook personBook = null;
            personServiceMock.Setup(x => x.GetPersonBookByBookId(It.IsAny<string>()))
                .Returns(personBook);

            var bookRepMock = new Mock<IEntityRepository<Book>>();
            Book book = null;
            bookRepMock.Setup(x=>x.GetSingle(It.IsAny<string>())).Returns(book);

            //act
            var libaryService = new Libraryervice(bookRepMock.Object, personServiceMock.Object);

            //assert
            Assert.Throws<DoesNotExistException>(() => libaryService.BorrowBook(string.Empty, string.Empty));
        }

        [Test]
        public void Throws_Exception_When_Book_Already_Borrowed()
        {
            //arrange
            //not null person
            var personServiceMock = new Mock<IPersonService>();
            Person person = new Person();
            personServiceMock.Setup(x => x.GetPerson(It.IsAny<string>()))
                .Returns(person);

            //not null book
            var bookRepMock = new Mock<IEntityRepository<Book>>();
            Book book = new Book();
            bookRepMock.Setup(x => x.GetSingle(It.IsAny<string>())).Returns(book);

            //but book borrowed
            PersonBook personBook = new PersonBook
            {
                Person = new Person()
            };
            personServiceMock.Setup(x => x.GetPersonBookByBookId(It.IsAny<string>()))
                .Returns(personBook);

            //act
            var libaryService = new Libraryervice(bookRepMock.Object, personServiceMock.Object);

            //assert
            Assert.Throws<NotAvailableToBorrowException>(() => libaryService.BorrowBook(string.Empty, string.Empty));            
        }

        [Test]
        public void Should_Return_True_Person_Book_Exist_Book_Not_Borrowed()
        {
            //arrange
            //not null person
            var personServiceMock = new Mock<IPersonService>();
            Person person = new Person();
            personServiceMock.Setup(x => x.GetPerson(It.IsAny<string>()))
                .Returns(person);
            personServiceMock.Setup(x => x.CreatePersonBook(It.IsAny<PersonBook>()))
                .Returns(new PersonBook());

            //not null book
            var bookRepMock = new Mock<IEntityRepository<Book>>();
            Book book = new Book();
            bookRepMock.Setup(x => x.GetSingle(It.IsAny<string>())).Returns(book);

            //but book borrowed
            PersonBook personBook = null;
            personServiceMock.Setup(x => x.GetPersonBookByBookId(It.IsAny<string>()))
                .Returns(personBook);

            //act
            var libaryService = new Libraryervice(bookRepMock.Object, personServiceMock.Object);

            //assert
            Assert.IsTrue(libaryService.BorrowBook(string.Empty, string.Empty));            
        }
    }
}
