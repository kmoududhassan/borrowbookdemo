using System;
using System.Collections.Generic;

using BorrowBook.Domain.Entities;

namespace BorrowBookDemo.Models
{
    public class BookDetailModel
    {       
        public Book Book { get; set; }
        public IList<Person> Persons { get; set; }
        public string BorrowedTo { get; set; }        
    }
}