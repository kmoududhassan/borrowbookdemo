﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ViewFeatures;

using BorrowBookDemo.Models;
using BorrowBook.Domain.Exceptions;

namespace BorrowBookDemo.Filters
{
    public class BorrowExceptionFilter : ExceptionFilterAttribute
    {
        public BorrowExceptionFilter() : base() { }

        public override void OnException(ExceptionContext context)
        {
            if (context.Exception is ICustomException)
            {
                ICustomException customEx = context.Exception as ICustomException;

                context.ModelState.AddModelError(customEx.Key, context.Exception.Message);
                context.Result = new ViewResult
                {
                    ViewName = "BorrowError"
                };
            }
        }
    }
}