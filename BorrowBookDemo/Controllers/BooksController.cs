﻿using System;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;

using BorrowBookDemo.Models;
using BorrowBookDemo.Filters;
using BorrowBook.Domain.Services;
using BorrowBook.Domain.Exceptions;

namespace BorrowBookDemo.Controllers
{
    public class BooksController : Controller
    {
        private readonly ILibraryService _libraryService;
        private readonly IPersonService _personService;

        public BooksController(
            ILibraryService libraryService,
            IPersonService personService)
        {
            _libraryService = libraryService;
            _personService = personService;
        }
        public IActionResult Index()
        {
            return View(_libraryService.GetBooks());
        }

        public IActionResult Details(string id)
        {
            var book = _libraryService.GetBook(id);

            if (null != book)
            {
                var persons = _personService.GetPersons();

                var detailModel = new BookDetailModel
                {
                    Book = book,
                    Persons = persons,
                    BorrowedTo = _libraryService.GetBorrowedPersonId(book.Id)
                };
                return View(detailModel);
            }
            return View("NotFound");
        }

        [HttpPost]
        [BorrowExceptionFilter]
        public IActionResult Borrow(string bookId, string borrowedTo)
        {
            _libraryService.BorrowBook(bookId, borrowedTo);

            return RedirectToAction("Index");
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
