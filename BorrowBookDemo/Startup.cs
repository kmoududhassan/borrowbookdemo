﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using BorrowBook.Domain.Entities;
using BorrowBook.Domain.Migrations;
using BorrowBook.Domain.Services;

namespace BorrowBookDemo
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<DbContext, EntitiesContext>();
            services.AddDbContext<EntitiesContext>(opt => opt.UseInMemoryDatabase("BorrowBookDemo"));
            services.AddTransient<IEntityRepository<Book>, EntityRepository<Book>>();
            services.AddTransient<IEntityRepository<Person>, EntityRepository<Person>>();
            services.AddTransient<IEntityRepository<PersonBook>, EntityRepository<PersonBook>>();
            services.AddTransient<IBookSeeder, BookSeeder>();
            services.AddTransient<IPersonSeeder, PersonSeeder>();
            services.AddTransient<IPersonService, PersonService>();
            services.AddTransient<ILibraryService, Libraryervice>();
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IPersonSeeder personSeeder, IBookSeeder bookSeeder)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
                personSeeder.Seed();
                bookSeeder.Seed();
            }
            else
            {
                app.UseExceptionHandler("/Books/Error");
            }

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Books}/{action=Index}/{id?}");
            });
        }
    }
}
