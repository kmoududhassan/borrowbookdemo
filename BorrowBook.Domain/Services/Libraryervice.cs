﻿using System.Linq;
using System.Collections.Generic;

using BorrowBook.Domain.Entities;
using BorrowBook.Domain.Exceptions;

namespace BorrowBook.Domain.Services
{
    public class Libraryervice : ILibraryService
    {
        private readonly IEntityRepository<Book> _bookRepository;             
        private readonly IPersonService _personService;

        public Libraryervice(
            IEntityRepository<Book> bookRepository,              
            IPersonService personService)
        {
            _bookRepository = bookRepository;                      
            _personService = personService;
        }

        public Book GetBook(string id)
        {
            return _bookRepository.GetSingle(id);
        }

        public IList<Book> GetBooks()
        {
            return _bookRepository.GetAll().ToList();
        }

        public bool BorrowBook(string bookId, string personId)
        {
            if (null == _personService.GetPerson(personId))
            {
                throw new DoesNotExistException("The person does not exist!", null);
            }

            var book = _bookRepository.GetSingle(bookId);

            if (null == book)
            {
                throw new DoesNotExistException("The book does not exist", null);
            }

            if (null != GetBorrowedPerson(bookId))
            {
                throw new NotAvailableToBorrowException("The book is already borrowed by someone!", null);
            }            

            book.IsBorrowed = true;

            var personBook = _personService.CreatePersonBook(new PersonBook
            {                
                BookId = bookId,
                PersonId = personId,
            });

            return personBook != null;
        }

        public Person GetBorrowedPerson(string bookId)
        {
            var bookPerson = _personService.GetPersonBookByBookId(bookId);

            if (null != bookPerson)
            {
                return bookPerson.Person;
            }
            return null;
        }

        public string GetBorrowedPersonId(string bookId)
        {
            var person = GetBorrowedPerson(bookId);
            return null != person ? person.Id : string.Empty;
        }        
    }
}
