﻿using System;
using System.Text;
using System.Collections.Generic;

using BorrowBook.Domain.Entities;

namespace BorrowBook.Domain.Services
{
    public interface ILibraryService
    {
        IList<Book> GetBooks();        
        Book GetBook(string id);
        bool BorrowBook(string bookId, string personId);
        Person GetBorrowedPerson(string bookId);
        string GetBorrowedPersonId(string bookId);
    }
}
