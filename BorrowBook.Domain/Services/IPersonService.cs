﻿using System.Collections.Generic;
using BorrowBook.Domain.Entities;

namespace BorrowBook.Domain.Services
{
    public interface IPersonService
    {
        IList<Person> GetPersons();
        Person GetPerson(string id);
        PersonBook GetPersonBookByBookId(string bookId);
        PersonBook CreatePersonBook(PersonBook personBook);
    }
}
