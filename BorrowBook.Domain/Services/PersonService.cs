﻿using System;
using System.Linq;
using System.Collections.Generic;

using BorrowBook.Domain.Entities;

namespace BorrowBook.Domain.Services
{
    public class PersonService : IPersonService
    {
        private readonly IEntityRepository<PersonBook> _personBookRepository;
        private readonly IEntityRepository<Person> _personRepository;

        public PersonService(
            IEntityRepository<PersonBook> personBookRepository,
            IEntityRepository<Person> personRepository)
        {
            _personBookRepository = personBookRepository;
            _personRepository = personRepository;
        }

        public PersonBook GetPersonBookByBookId(string bookId)
        {
            return _personBookRepository.AllIncluding(x => x.Person, y => y.Book)
                    .FirstOrDefault(x => x.BookId == bookId);
        }

        public PersonBook CreatePersonBook(PersonBook personBook)
        {
            if (string.IsNullOrEmpty(personBook.Id) 
                || personBook.Id.ToString() == Guid.Empty.ToString())
            {
                personBook.Id = Guid.NewGuid().ToString();
            }
            _personBookRepository.Add(personBook);
            bool success = _personBookRepository.Save();

            return success ? personBook : null;
        }

        public IList<Person> GetPersons()
        {
            return _personRepository.GetAll().ToList();
        }

        public Person GetPerson(string id)
        {
            return _personRepository.GetSingle(id);
        }
    }
}
