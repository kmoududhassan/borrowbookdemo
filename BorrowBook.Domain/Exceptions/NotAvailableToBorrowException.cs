﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BorrowBook.Domain.Exceptions
{
    public class NotAvailableToBorrowException : Exception, ICustomException
    {
        public string Key
        {
            get
            {
                return "NotAvailableToBorrowException";
            }
        }
        public NotAvailableToBorrowException() { }
        public NotAvailableToBorrowException(string message, Exception ex) : base(message, ex) { }
    }
}
