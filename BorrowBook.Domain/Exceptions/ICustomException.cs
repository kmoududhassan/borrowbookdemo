﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BorrowBook.Domain.Exceptions
{
    public interface ICustomException
    {
        string Key { get; }
    }
}
