﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BorrowBook.Domain.Exceptions
{
    public class DoesNotExistException : Exception, ICustomException
    {
        public string Key
        {
            get
            {
                return "DoesNotExistException";
            }
        }        

        public DoesNotExistException() { }
        public DoesNotExistException(string message, Exception ex) : base(message, ex) { }
    }
}
