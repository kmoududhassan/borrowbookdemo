﻿using System;
using System.Text;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BorrowBook.Domain.Entities
{
    public class Person : IEntity
    {
        [Key]
        public string Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }
        
        public virtual IList<PersonBook> PersonBooks { get; set; }
    }
}
