﻿using System;
using System.Text;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BorrowBook.Domain.Entities
{
    public class PersonBook : IEntity
    {
        [Key]
        public string Id { get; set; }

        [ForeignKey("PersonId")]
        public string PersonId { get; set; }

        [ForeignKey("BookId")]
        public string BookId { get; set; }

        public virtual Person Person { get; set; }

        public virtual Book Book { get; set; }
    }
}
