﻿using System;
using System.Text;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BorrowBook.Domain.Entities
{
    public class Book : IEntity
    {
        [Key]
        public string Id { get; set; }

        public string Title { get; set; }

        public string Genre { get; set; }

        public decimal Price { get; set; }

        public DateTime PublishDate { get; set; }

        public string Description { get; set; }        

        public bool IsBorrowed { get; set; }

        public virtual PersonBook PersonBook { get; set; }

        public Book()
        {
            IsBorrowed = false;
        }
    }
}
