﻿using System;

namespace BorrowBook.Domain.Entities
{
    public interface IEntity
    {
        string Id { get; set; }
    }
}