﻿using Microsoft.EntityFrameworkCore;

namespace BorrowBook.Domain.Entities
{
    public class EntitiesContext : DbContext
    {
        public EntitiesContext(DbContextOptions<EntitiesContext> options) : base(options) { }

        public DbSet<Book> Books { get; set; }
        public DbSet<Person> Persons { get; set; }
        public DbSet<PersonBook> PersonBooks { get; set; }
    }
}