﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BorrowBook.Domain.Migrations
{
    public interface ISeeder
    {
        void Seed();
        bool Empty();
    }
}
