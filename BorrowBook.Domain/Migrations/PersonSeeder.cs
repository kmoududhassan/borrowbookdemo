﻿using System.Linq;

using BorrowBook.Domain.Entities;

namespace BorrowBook.Domain.Migrations
{
    public class PersonSeeder : IPersonSeeder
    {
        private readonly IEntityRepository<Person> _personRepository;        

        public PersonSeeder(IEntityRepository<Person> personRepository)
        {
            _personRepository = personRepository;
        }

        public bool Empty()
        {
            return _personRepository.GetAll().Count() == 0;            
        }

        public void Seed()
        {
            if (!Empty())
            {
                return;
            }

            _personRepository.Add(new Person
            {
                FirstName = "Moudud",
                LastName = "Hassan",
                Id = "Person01"
            });

            _personRepository.Add(new Person
            {
                FirstName = "Scott",
                LastName = "Hanselman",
                Id = "Person02"
            });

            _personRepository.Add(new Person
            {
                FirstName = "Scott",
                LastName = "Guthrie",
                Id = "Person03"
            });

            _personRepository.Save();
        }
    }
}
