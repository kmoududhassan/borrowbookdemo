﻿using System;
using System.Linq;
using System.Xml.Linq;
using Microsoft.AspNetCore.Hosting;

using BorrowBook.Domain.Entities;
using System.IO;

namespace BorrowBook.Domain.Migrations
{
    public class BookSeeder : IBookSeeder
    {
        private readonly IEntityRepository<Book> _bookRepository;
        private readonly IHostingEnvironment _hostingEnvironment;

        public BookSeeder(
            IHostingEnvironment hostingEnvironment, 
            IEntityRepository<Book> bookRepository)
        {
            _bookRepository = bookRepository;
            _hostingEnvironment = hostingEnvironment;
        }

        public bool Empty()
        {
            return _bookRepository.GetAll().Count() == 0;            
        }

        public void Seed()
        {
            if (!Empty())
            {
                return;
            }

            string xmlPath = Path.Combine(_hostingEnvironment.WebRootPath, "data");
            xmlPath = Path.Combine(xmlPath, "catalog.xml");
            XDocument xdoc = XDocument.Load(xmlPath);

            var books = (from book in xdoc.Descendants("book")
                        select new Book
                        {
                            Id = book.Attribute("id").Value,
                            Title = book.Element("title").Value,
                            IsBorrowed = false,
                            Price = Convert.ToDecimal(book.Element("price").Value),
                            PublishDate = Convert.ToDateTime(book.Element("publish_date").Value),
                            Genre = book.Element("genre").Value,
                            Description = book.Element("description").Value,
                        }).ToList();

            foreach (var book in books)
                _bookRepository.Add(book);
                        
            _bookRepository.Save();
        }
    }
}
